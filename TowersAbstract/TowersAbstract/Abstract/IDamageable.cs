namespace TowersAbstract.Abstract;

public interface IDamageable
{
    public string Name { get; }
    
    public int Health { get; }

    public void TakeDamage(int damage);
}
namespace TowersAbstract.Abstract;

public interface IAttacker
{
    public int Damage { get; }
    public int Radius { get; }
    
    public void Attack(IDamageable target);
}
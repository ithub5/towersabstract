using TowersAbstract.Abstract;
using TowersAbstract.Data;

namespace TowersAbstract.Entities;

public class Tower : IAttacker, IDamageable
{
    public string Name { get; }
    public EntityGeneralSettings Settings { get; private set; }
    public int Health { get; }
    public int Damage { get; }
    public int Radius { get; }

    private int _healthCounter;

    public event Action<string> OnAttack;
    public event Action<string> OnHealthChanged;
    public event Action<string> OnDeath;
    
    public Tower(string name, EntityGeneralSettings settings)
    {
        Name = name;
        Settings = settings;
        Health = Settings.Health;
        Damage = Settings.Damage;
        Radius = Settings.Radius;
        _healthCounter = Health;
    }

    public void Attack(IDamageable target)
    {
        OnAttack?.Invoke($"{Name} Attacks {target.Name}");
        target.TakeDamage(Damage);
    }


    public void TakeDamage(int damage)
    {
        _healthCounter -= damage;
        OnHealthChanged?.Invoke($"{Name} Health: {_healthCounter}");

        if (_healthCounter <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        OnDeath?.Invoke($"{Name} is Destroyed");
    }
}
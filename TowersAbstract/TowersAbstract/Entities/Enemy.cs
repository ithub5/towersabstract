using TowersAbstract.Abstract;
using TowersAbstract.Data;

namespace TowersAbstract.Entities;

public class Enemy : IAttacker, IDamageable
{
    public string Name { get; }
    public EnemySettings Settings { get; }
    public int Health { get; }
    public int Damage { get; }
    public int Radius { get; }
    public float HealPercentage { get; }

    public Point CurrentPos => _wayPoints[_nextWayPointIndex];
    public Point NextPos => _wayPoints[_nextWayPointIndex + 1 < _wayPoints.Count ? _nextWayPointIndex + 1 : 0];
    
    private int _healthCounter;
    
    private List<Point> _wayPoints;
    private int _nextWayPointIndex;

    private bool _arrived;

    public event Action<string> OnAttack;
    public event Action<string> OnHealthChanged;
    public event Action<string> OnDeath;
    public event Action<string> OnArrived;

    public Enemy(string name, EnemySettings settings)
    {
        Name = name;
        Settings = settings;
        Health = Settings.Health;
        Damage = Settings.Damage;
        Radius = Settings.Radius;
        HealPercentage = Settings.HealPercentage;
        _healthCounter = Health;

        _wayPoints = new List<Point>();
    }

    public void AddWayPoints(params Point[] newPoints)
    {
        foreach (var newPoint in newPoints)
        {
            _wayPoints.Add(newPoint);
        }
    }

    public void Move()
    {
        if (_arrived)
        {
            return;
        }
        _nextWayPointIndex++;
        if (_nextWayPointIndex >= _wayPoints.Count)
        {
            OnArrived?.Invoke($"{Name} Arrived");
            _arrived = true;
            _nextWayPointIndex--;
        }
    }

    public void Attack(IDamageable target)
    {
        OnAttack?.Invoke($"{Name} Attacks {target.Name}");
        target.TakeDamage(Damage);
    }

    public void TakeDamage(int damage)
    {
        _healthCounter -= damage;
        int healthToHeal = (int)(Health * HealPercentage);
        _healthCounter = _healthCounter + healthToHeal > Health ? Health : _healthCounter + healthToHeal;
        OnHealthChanged?.Invoke($"{Name} Health: {_healthCounter}");

        if (_healthCounter <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        OnDeath?.Invoke($"{Name} is Dead");
    }
}
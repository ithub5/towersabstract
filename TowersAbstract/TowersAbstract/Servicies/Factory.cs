using System.Drawing;
using TowersAbstract.Abstract;
using TowersAbstract.Data;
using TowersAbstract.Entities;
using TowersAbstract.Utils;

namespace TowersAbstract.Servicies;

public  static class Factory
{
    
    
    public static Tower CreateTower(string name, EntityGeneralSettings settings)
    {
        return new Tower(name, settings);
    }

    public static Enemy CreateEnemy(string name, EnemySettings settings)
    {
        return new Enemy(name, settings);
    }
}
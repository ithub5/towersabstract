namespace TowersAbstract.Utils;

public static class MyExtensions
{
    public static void ForEach<T>(this T[,] collection, Action<int, int> action)
    {
        for (int x = 0; x < collection.GetLength(0); x++)
        {
            for (int y = 0; y < collection.GetLength(1); y++)
            {
                action?.Invoke(x, y);
            }
        }
    }
}
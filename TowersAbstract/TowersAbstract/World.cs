using System.Numerics;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using TowersAbstract.Abstract;
using TowersAbstract.Data;
using TowersAbstract.Utils;

namespace TowersAbstract.Entities;

public class World
{
    private WorldCell[,] _grid;

    private Dictionary<Point, Enemy> _enemiesToMove;

    // is made for not allowing enemy to travel through multiple waypoints on one turn, kind of a spike moment
    // but still there is only one enemy would work
    private Point _ignorePoint;
    private bool _doIgnorePoint;

    public World(int x, int y)
    {
        _grid = new WorldCell[x, y];
        _grid.ForEach((x, y) => _grid[x, y] = new WorldCell());
    }
    
    public void SetCell(object Object, int x, int y)
    {
        _grid[x, y].SetNewObject(Object);
    }

    public void EmptyCell(int x, int y)
    {
        _grid[x, y].SetNewObject(null);
    }

    public void PerformTurn()
    {
        _grid.ForEach(TurnForCell);
        _doIgnorePoint = false;
    }

    private void TurnForCell(int x, int y)
    {
        if (_grid[x, y].IsEmpty || (_doIgnorePoint && (_ignorePoint.x == x && _ignorePoint.y == y)))
        {
            return;
        }

        object occupant = _grid[x, y].Occupant;
        Type occupantType = _grid[x, y].OccupantType;
        bool towerEnemy = occupantType == typeof(Tower);
        Type targetType = towerEnemy ? typeof(Enemy) : typeof(Tower);

        if (!towerEnemy)
        {
            Enemy enemy = (Enemy)occupant;
            Move(x, y, enemy);
            _ignorePoint = enemy.CurrentPos;
            _doIgnorePoint = true;
        }
        AttackOnRadius(x, y, (IAttacker)occupant, targetType);
    }

    private void Move(int x, int y, Enemy enemy)
    {
        (_grid[x, y], _grid[enemy.NextPos.x, enemy.NextPos.y]) = (_grid[enemy.NextPos.x, enemy.NextPos.y], _grid[x, y]);
        enemy.Move();
    }

    private void AttackOnRadius(int x, int y, IAttacker attacker, Type mask)
    {
        int radius = attacker.Radius;
        int leftBorder = x - radius >= 0 ? x - radius : 0;
        int rightBorder = x + radius < _grid.GetLength(0) ? x + radius : _grid.GetLength(0) - 1;
        int topBorder = y - radius >= 0 ? y - radius : 0;
        int bottomBorder = y + radius < _grid.GetLength(1) ? y + radius : _grid.GetLength(1) - 1;

        for (int i = leftBorder; i <= rightBorder; i++)
        {
            for (int j = topBorder; j <= bottomBorder; j++)
            {
                if (_grid[i, j].IsEmpty || _grid[i, j].OccupantType != mask)
                {
                    continue;
                }
                
                attacker.Attack((IDamageable)_grid[i, j].Occupant);
            }
        }
    }
}
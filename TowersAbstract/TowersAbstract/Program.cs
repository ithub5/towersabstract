﻿using System.IO.Pipes;
using TowersAbstract.Data;
using TowersAbstract.Entities;

namespace TowersAbstract;

class Program
{
    private static void Main()
    {
        World world = new World(7, 7);
        EntityGeneralSettings towerSettings = new EntityGeneralSettings(10, 3, 1);
        EnemySettings enemySettings = new EnemySettings(10, 2, 1, .1f);
        
        Enemy enemy = new Enemy("BOB", enemySettings);
        enemy.AddWayPoints(new Point(0,2), new Point(1,2), new Point(2,2), new Point(3,2), new Point(3,3), new Point(3,4),
            new Point(4, 4), new Point(5, 4), new Point(6, 4));
        Tower tower = new Tower("BOBsKiller", towerSettings);
        
        world.SetCell(enemy, enemy.CurrentPos.x, enemy.CurrentPos.y);
        world.SetCell(tower, 2, 3);

        UI ui = new UI();
        enemy.OnArrived += ui.LogMassage;
        enemy.OnAttack += ui.LogMassage;
        enemy.OnHealthChanged += ui.LogMassage;
        enemy.OnDeath += ui.LogMassage;
        tower.OnAttack += ui.LogMassage;
        tower.OnHealthChanged += ui.LogMassage;
        tower.OnDeath += ui.LogMassage;
        
        bool enemyDeadOrArrived = false;

        enemy.OnArrived += (text) => enemyDeadOrArrived = true;
        enemy.OnDeath += (text) => enemyDeadOrArrived = true;

        while (!enemyDeadOrArrived)
        {
            Thread.Sleep(1000);
            ui.Clear();
            world.PerformTurn();
            Console.WriteLine($"{enemy.CurrentPos.x}, {enemy.CurrentPos.y}");
        }
    }
}


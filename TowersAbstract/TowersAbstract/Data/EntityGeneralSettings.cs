namespace TowersAbstract.Data;

public class EntityGeneralSettings
{
    public int Health { get; }
    public int Damage { get; }
    public int Radius { get; }

    public EntityGeneralSettings(int health, int damage, int radius)
    {
        Health = health;
        Damage = damage;
        Radius = radius;
    }
}
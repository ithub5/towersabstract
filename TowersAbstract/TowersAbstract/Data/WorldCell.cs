namespace TowersAbstract.Data;

public class WorldCell
{
    public object Occupant { get; private set; }
    public Type OccupantType { get; private set; }

    public bool IsEmpty => Occupant == null;

    public void SetNewObject(object newOccupant)
    {
        Occupant = newOccupant;
        
        if (newOccupant != null)
        {
            OccupantType = Occupant.GetType();
        }
        else
        {
            OccupantType = null;
        }
    }
}
namespace TowersAbstract.Data;

public class EnemySettings : EntityGeneralSettings
{
    public float HealPercentage;
    
    public EnemySettings(int health, int damage, int radius, float healPercentage) : base(health, damage, radius)
    {
        HealPercentage = healPercentage;
    }
}
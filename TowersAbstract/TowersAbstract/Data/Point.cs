namespace TowersAbstract.Data;

public struct Point
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public override bool Equals(object obj)
    {
        Point point = (Point)obj;

        return point.x == x && point.y == y;
    }
}